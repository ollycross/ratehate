<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/', '\App\Http\Controllers\RaterController@index')->name('home');
    Route::post('/', '\App\Http\Controllers\RaterController@processSubmission');

    Route::get('/{page}', '\App\Http\Controllers\PageController@index')->name('page');

});

<?php

namespace Database\Seeders;

use App\Models\DemographicType;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DummyUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory('1000')->create()->each(function (User $user) {
            DemographicType::each(function (DemographicType $demographicType) use ($user) {
                $demographics = $demographicType->demographics->all();
                $callable = [$user, Str::camel($demographicType->slug)];
                $callable()->save($demographics[array_rand($demographics)]);
                $user->save();
            });
        });
    }
}

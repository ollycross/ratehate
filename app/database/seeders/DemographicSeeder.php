<?php

namespace Database\Seeders;

use App\Models\Demographic;
use App\Models\DemographicType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class DemographicSeeder extends Seeder
{
    private const CONFIG_FILE = 'demographics.yml';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getDemographics() as $order => $demographicType) {
            foreach ($demographicType as $name => $demographics) {
                $demographicType = $this->getDemographicType($name, $order);
                array_map(
                    $this->createDemographicFactory($demographicType),
                    $demographics,
                    array_keys($demographics)
                );
            }
        }
    }

    private function createDemographicFactory(DemographicType $demographicType): callable
    {
        return function (string $name, int $order) use ($demographicType) {
            $this->createDemographic($demographicType, $name, $order);
        };
    }

    private function createDemographic(DemographicType $demographicType, string $name, int $order): void
    {
        Demographic::firstOrCreate([
            'slug' => Str::slug($name),
            'demographic_type_id' => $demographicType->id
        ], [
            'name' => $name,
            'order' => $order
        ]);

        $this->command->info(sprintf('Created Demographic \'%s - %s\'', $demographicType->name, $name));
    }

    private function getDemographicType(string $name, int $order): DemographicType
    {

        $demographicType =  DemographicType::firstOrCreate([
            'slug' => Str::slug($name)
        ], [
            'name' => $name,
            'order' => $order
        ]);
        $this->command->info(sprintf('Created Demographic \'%s\'', $name));

        return $demographicType;
    }

    private function getDemographics()
    {
        return Yaml::parseFile(config_path(self::CONFIG_FILE));
    }
}

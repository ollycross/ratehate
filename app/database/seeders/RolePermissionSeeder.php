<?php

namespace Database\Seeders;

use App\Sharp\Auth\SharpCheckHandler;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    private const PERMISSIONS = [
        SharpCheckHandler::REQUIRED_PERMISSION
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $admin = Role::firstOrCreate(['name' => 'admin']);
        foreach (self::PERMISSIONS as $permissionName) {
            $permission = Permission::firstOrCreate(['name' => $permissionName]);
            $permission->assignRole($admin);
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Classification;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class ClassificationSeeder extends Seeder
{
    private const CONFIG_FILE = 'classifications.yml';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getClassifications() as $order => $classification) {
            Classification::firstOrCreate([
                'slug' => $classification['slug'] ?? Str::slug($classification['name'])
                ], [
                    'order' => $order,
                    'name' => $classification['name'],
                    'description' => $classification['description'] ?? null,
                ]);

            $this->command->info(sprintf('Created Classification \'%s\'', $classification['name']));
        }
    }

    private function getClassifications(): array
    {
        return Yaml::parseFile(config_path(self::CONFIG_FILE));
    }
}

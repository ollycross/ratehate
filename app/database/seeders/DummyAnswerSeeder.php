<?php

namespace Database\Seeders;

use App\Models\Answer;
use App\Models\Classification;
use App\Models\Comment;
use App\Models\Highlight;
use App\Models\OtherValue;
use App\Models\Text;
use App\Models\User;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DummyAnswerSeeder extends Seeder
{
    private Generator $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Factory::create();
        for ($i = 0; $i < 1000; ++$i) {
            $this->makeOneAnswer();
        }
    }

    private function oneIn(int $total): bool
    {
        return rand(1, $total) === 1;
    }

    private function addComment(Answer $answer): Comment
    {
        return Comment::create([
            'comment' => $this->faker->text(200),
            'commentable_type' => Text::class,
            'commentable_id' => $answer->text_id,
            'user_id' => $answer->user_id,
        ]);
    }


    private function randomModelBuilder(string $modelClass): Builder
    {
        $callable = sprintf('%s::select', $modelClass);
        return $callable('id')->inRandomOrder()->limit(1);
    }

    private function randomModel(string $modelClass): Model
    {
        return $this->randomModelBuilder($modelClass)->first();
    }

    private function randomModelId(string $modelClass): int
    {
        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        return $this->randomModel($modelClass)->id;
    }

    private function makeOneAnswer()
    {
        $answer = Answer::create([
            'text_id' => $this->randomModelId(Text::class),
            'user_id' => $this->randomModelId(User::class),
        ]);

        if ($this->oneIn(10)) {
            $this->addComment($answer);
        }

        if (!$this->oneIn(10)) {
            $this->addSomeClassifications($answer);
        }

        if (!$this->oneIn(5)) {
            $this->addSomeHighlights($answer, 4);
        }
    }

    private function addSomeClassifications(Answer $answer): void
    {
        if ($this->oneIn(2)) {
            $answer
                ->classifications()
                ->save(
                    $this->randomModelBuilder(Classification::class)
                        ->where('slug', '!=', 'other')
                        ->first()
                );
            return;
        }

        if ($this->oneIn(5)) {
            $other = Classification::firstWhere('slug', 'other');
            $answer
                ->classifications()
                ->save($other);

            OtherValue::create([
                'value' => $this->faker->text(20),
                'otherable_type' => Classification::class,
                'otherable_id' => $other->id,
                'relatable_id' => $answer->id
            ]);

            return;
        }

        $limit = rand(2, Classification::where('slug', '!=', 'other')->get()->count());
        $answer->classifications()->saveMany($this->randomModelBuilder(Classification::class)->limit($limit)->get());
    }

    private function getTextSnippet(string $text, int $startPos = 0): ?array
    {
        $start = rand($startPos, strlen($text));
        $end = rand($start, strlen($text));

        if ($start === $end) {
            return null;
        }

        return [$start, $end];
    }

    private function addSomeHighlights(Answer $answer, int $oneIn, $startPos = 0): void
    {
        if ($this->oneIn($oneIn)) {
            $boundaries = $this->getTextSnippet($answer->text->formatted_text, $startPos);
            if (!$boundaries) {
                return;
            }

            [$start, $end] = $boundaries;

            Highlight::create([
                'answer_id' => $answer->id,
                'start' => $start,
                'end' => $end
            ]);

            $this->addSomeHighlights($answer, $oneIn * 2, $start);
        }
    }
}

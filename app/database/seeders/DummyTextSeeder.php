<?php

namespace Database\Seeders;

use App\Models\Text;
use Illuminate\Database\Seeder;

class DummyTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Text::factory(1000)->create([
            'source' => 'seeder',
        ]);
    }
}

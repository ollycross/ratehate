<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $email = env('ADMIN_USER_EMAIL') ?: $faker->unique()->safeEmail;
        $password = env('ADMIN_USER_PASSWORD') ?: $faker->password(8, 12);

        $user = User::firstOrNew(
            [
                'name' => 'Admin user'
            ],
            [
                'email' => $email,
                'email_verified_at' => now(),
                'password' => Hash::make($password),
                'remember_token' => Str::random(10),
            ]
        );

        if (!$user->id) {
            $user->save();
            $user->assignRole('admin');

            $this->command->info(
                sprintf('Created admin user with email \'%s\' and password \'%s\'.', $email, $password)
            );

            return;
        }

        $this->command->warn(
            sprintf('Admin user already exists (email \'%s\').', $user->email)
        );
    }
}

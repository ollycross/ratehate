<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_values', function (Blueprint $table) {
            $table->id();
            $table->string('value');
            $table->string('otherable_type');
            $table->unsignedInteger('otherable_id');
            $table->unsignedInteger('relatable_id');
            $table->timestamps();

            $table->unique(['relatable_id', 'otherable_type', 'otherable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_values');
    }
}

<?php

use App\Models\Classification;
use App\Models\Demographic;
use App\Models\DemographicType;
use Illuminate\Database\Migrations\Migration;

class PopulateRequiredData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('db:seed', ['--class' => 'DemographicSeeder']);
        Artisan::call('db:seed', ['--class' => 'ClassificationSeeder']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Demographic::truncate();
        DemographicType::truncate();
        Classification::truncate();
        Schema::enableForeignKeyConstraints();
    }
}

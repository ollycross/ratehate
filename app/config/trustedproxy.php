<?php

return array_filter(
    array_map(
        'trim',
        explode(',', env('TRUSTED_PROXIES') ?: '')
    )
);

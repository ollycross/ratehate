<?php

use App\Sharp\Page\PageForm;
use App\Sharp\Page\PageSharpList;
use App\Sharp\Page\PageValidator;
use App\Sharp\Text\TextSharpList;

return [
    "menu" => [
        [
            "label" => "Pages",
            "icon" => "fa-info",
            "entity" => "page"
        ],
        [
            "label" => "Text fragments",
            "icon" => "fa-file-text",
            "entity" => "text"
        ]
    ],
    'entities' => [
        'text' => [
            'list' => TextSharpList::class,
        ],
        'page' => [
            'list' => PageSharpList::class,
            'form' => PageForm::class,
            'validator' => PageValidator::class
        ]
    ],
    'auth' => [
        'check_handler' => App\Sharp\Auth\SharpCheckHandler::class
    ]
];

<?php

/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Highlight
 *
 * @property-read Answer $answer
 * @property-read Text|null $text
 * @method static Builder|Highlight newModelQuery()
 * @method static Builder|Highlight newQuery()
 * @method static Builder|Highlight query()
 * @mixin Eloquent
 * @property int $id
 * @property int $start
 * @property int $end
 * @property int $answer_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Highlight whereAnswerId($value)
 * @method static Builder|Highlight whereCreatedAt($value)
 * @method static Builder|Highlight whereEnd($value)
 * @method static Builder|Highlight whereId($value)
 * @method static Builder|Highlight whereStart($value)
 * @method static Builder|Highlight whereUpdatedAt($value)
 * @property-read string $snippet
 */
class Highlight extends Model
{
    use HasFactory;

    protected $fillable = [
        'start',
        'end',
        'answer_id'
    ];

    protected $appends = [
        'snippet',
        'user'
    ];

    public function answer(): BelongsTo
    {
        return $this->belongsTo(Answer::class);
    }

    public function text(): BelongsTo
    {
        return $this->answer->text();
    }

    public function user(): BelongsTo
    {
        return $this->answer->user();
    }

    public function getSnippetAttribute(): string
    {
        $text = $this->text;

        return substr($text->text, $this->start, $this->end - $this->start);
    }

    public static function fromArray(array $array, int $answerId): self
    {
        [$start, $end] = $array;
        return Highlight::firstOrCreate([
            'start' => $start,
            'end' => $end,
            'answer_id' => $answerId,
        ]);
    }
}

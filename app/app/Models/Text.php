<?php

namespace App\Models;

use App\Traits\HasComment;
use Eloquent;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Text
 *
 * @property int $id
 * @property string $text
 * @property string $source
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Answer[] $answers
 * @property-read int|null $answers_count
 * @method static Builder|Text newModelQuery()
 * @method static Builder|Text newQuery()
 * @method static Builder|Text query()
 * @method static Builder|Text whereCreatedAt($value)
 * @method static Builder|Text whereId($value)
 * @method static Builder|Text whereSource($value)
 * @method static Builder|Text whereText($value)
 * @method static Builder|Text whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string|null $source_id
 * @property-read Comment|null $comment
 * @property-read mixed $formatted_text
 * @property-read Collection|Incomprehensible[] $incomprehensibles
 * @property-read int|null $incomprehensibles_count
 * @method static Builder|Text whereSourceId($value)
 */
class Text extends Model
{
    use HasFactory;
    use HasComment;

    protected $fillable = [
        'text',
        'source',
        'source_id',
    ];

    protected $appends = [
        'formatted_text',
    ];

    public function getFormattedTextAttribute()
    {
        return nl2br(strip_tags($this->text));
    }

    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }

    public function comments()
    {
    }

    public function incomprehensibles(): HasMany
    {
        return $this->hasMany(Incomprehensible::class);
    }

    public static function sharpList(): LengthAwarePaginator
    {
        return static::paginate();
    }

}

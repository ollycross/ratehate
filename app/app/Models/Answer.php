<?php

/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models;

use App\Traits\HasComment;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Answer
 *
 * @property int $id
 * @property int $user_id
 * @property int $text_id
 * @property int|null $classification_id
 * @property string|null $highlights
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Classification|null $classification
 * @property-read Text|null $text
 * @property-read User|null $user
 * @method static Builder|Answer newModelQuery()
 * @method static Builder|Answer newQuery()
 * @method static Builder|Answer query()
 * @method static Builder|Answer whereClassificationId($value)
 * @method static Builder|Answer whereCreatedAt($value)
 * @method static Builder|Answer whereHighlights($value)
 * @method static Builder|Answer whereId($value)
 * @method static Builder|Answer whereTextId($value)
 * @method static Builder|Answer whereUpdatedAt($value)
 * @method static Builder|Answer whereUserId($value)
 * @mixin Eloquent
 * @property-read int|null $classification_count
 * @property-read int|null $highlights_count
 * @property-read Collection|Classification[] $classifications
 * @property-read int|null $classifications_count
 * @property-read array $classification_names
 * @property-read Comment|null $comment
 */
class Answer extends Model
{
    use HasFactory;
    use HasComment;

    protected $appends = [
        'classification_names',
    ];

    protected $fillable = [
        'user_id',
        'text_id',
        'classification_id',
        'highlights',
    ];

    public function text(): BelongsTo
    {
        return $this->belongsTo(Text::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function classifications(): BelongsToMany
    {
        return $this->belongsToMany(Classification::class)->withTimestamps();
    }

    /** @noinspection PhpUnused - Is used by magic methods */
    public function getClassificationNamesAttribute(): array
    {
        return $this->classifications->map(function ($classification) {
            if (!$classification->isOther()) {
                return $classification->name;
            }

            return sprintf(
                'Other: %s',
                $classification->otherValue($this->id)->first()->value ?? $classification->name
            );
        })->all();
    }

    public function highlights(): HasMany
    {
        return $this->hasMany(Highlight::class);
    }

    public static function fromRater(
        int $userId,
        int $textId,
        array $classificationSlugs = [],
        ?string $classificationOther = null
    ): Answer {
        $answer = Answer::updateOrCreate([
            'user_id' => $userId,
            'text_id' => $textId,
        ]);

        self::attachClassificationsToAnswer($answer, $classificationSlugs, $classificationOther);

        return $answer;
    }

    public static function attachClassificationsToAnswer(Answer $answer, array $slugs, ?string $other = null)
    {
        $classifications = Classification::whereIn('slug', $slugs)->get();

        if ($other && $otherClassification = $classifications->firstWhere('slug', 'other')) {
            OtherValue::fromData($other, $answer->id, $otherClassification);
        }

        $classificationIds = $classifications->pluck('id')->all();

        $answer->classifications()->syncWithoutDetaching($classificationIds);
    }


}

<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\DemographicType
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Demographic[] $demographics
 * @property-read int|null $demographics_count
 * @method static Builder|DemographicType newModelQuery()
 * @method static Builder|DemographicType newQuery()
 * @method static Builder|DemographicType query()
 * @method static Builder|DemographicType whereCreatedAt($value)
 * @method static Builder|DemographicType whereId($value)
 * @method static Builder|DemographicType whereName($value)
 * @method static Builder|DemographicType whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $slug
 * @method static Builder|DemographicType whereSlug($value)
 * @property int $order
 * @method static Builder|DemographicType whereOrder($value)
 */
class DemographicType extends Model
{
    use HasFactory;

    public function demographics(): HasMany
    {
        return $this->hasMany(Demographic::class);
    }
}

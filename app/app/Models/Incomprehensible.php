<?php

/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * App\Models\Incomprehensible
 *
 * @property-read Text|null $text
 * @property-read User|null $user
 * @method static Builder|Incomprehensible newModelQuery()
 * @method static Builder|Incomprehensible newQuery()
 * @method static Builder|Incomprehensible query()
 * @mixin Eloquent
 * @property int $id
 * @property int $text_id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Incomprehensible whereCreatedAt($value)
 * @method static Builder|Incomprehensible whereId($value)
 * @method static Builder|Incomprehensible whereTextId($value)
 * @method static Builder|Incomprehensible whereUpdatedAt($value)
 * @method static Builder|Incomprehensible whereUserId($value)
 */
class Incomprehensible extends Model
{
    use HasFactory;

    protected $fillable = [
        'text_id',
        'user_id'
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class);
    }

    public function text(): HasOne
    {
        return $this->hasOne(Text::class);
    }
}

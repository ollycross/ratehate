<?php

/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models;

use App\Interfaces\HasOtherValue;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\OtherValue
 *
 * @property-read mixed $other_value
 * @property-read Model|Eloquent $otherable
 * @method static Builder|OtherValue newModelQuery()
 * @method static Builder|OtherValue newQuery()
 * @method static Builder|OtherValue query()
 * @mixin Eloquent
 * @property int $id
 * @property string $value
 * @property string $otherable_type
 * @property int $otherable_id
 * @property int $relatable_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|OtherValue whereCreatedAt($value)
 * @method static Builder|OtherValue whereId($value)
 * @method static Builder|OtherValue whereOtherableId($value)
 * @method static Builder|OtherValue whereOtherableType($value)
 * @method static Builder|OtherValue whereRelatableId($value)
 * @method static Builder|OtherValue whereUpdatedAt($value)
 * @method static Builder|OtherValue whereValue($value)
 */
class OtherValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'other',
        'user_id',
    ];

    public function otherable()
    {
        return $this->morphTo();
    }

    /**
     * @param string $value
     * @param int $relatableId
     * @param Model $attachTo
     * @return OtherValue
     */
    public static function fromData(string $value, int $relatableId, Model $attachTo): self
    {
        $otherValue = new self([
            'value' => $value,
            'relatable_id' => $relatableId
        ]);

        $otherValue->otherable()->associate($attachTo);

        return $otherValue;
    }
}

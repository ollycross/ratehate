<?php

/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models;

use App\Traits\HasOtherValue;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Demographic
 *
 * @property int $id
 * @property int $type_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read DemographicType $type
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Demographic newModelQuery()
 * @method static Builder|Demographic newQuery()
 * @method static Builder|Demographic query()
 * @method static Builder|Demographic whereCreatedAt($value)
 * @method static Builder|Demographic whereId($value)
 * @method static Builder|Demographic whereTypeId($value)
 * @method static Builder|Demographic whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $name
 * @property string $slug
 * @property int $order
 * @property int $demographic_type_id
 * @method static Builder|Demographic whereDemographicTypeId($value)
 * @method static Builder|Demographic whereName($value)
 * @method static Builder|Demographic whereOrder($value)
 * @method static Builder|Demographic whereSlug($value)
 * @property-read DemographicType $demographicType
 * @property-read OtherValue|null $otherValue
 */
class Demographic extends Model
{
    use HasFactory;
    use HasOtherValue {
        HasOtherValue::fromSlug as traitFromSlug;
    }

    /**
     * @var string[]
     */
    protected $appends = [
        'type',
    ];

    public function getTypeAttribute(): string
    {
        return $this->demographicType->slug;
    }

    public function demographicType(): BelongsTo
    {
        return $this->belongsTo(DemographicType::class);
    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class);
    }

    public static function fromSlug(string $demographicTypeSlug, string $slug, int $relatableId = null)
    {
        $demographicType = DemographicType::firstWhere('slug', $demographicTypeSlug);
        $builder = self::whereDemographicTypeId($demographicType->id);

        return self::traitFromSlug($slug, $builder, $relatableId);
    }
}

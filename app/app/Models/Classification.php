<?php

namespace App\Models;

use App\Traits\HasOtherValue;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Carbon;

/**
 * App\Models\Classification
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Answer[] $answers
 * @property-read int|null $answers_count
 * @property-read Collection|Text[] $texts
 * @property-read int|null $texts_count
 * @method static Builder|Classification newModelQuery()
 * @method static Builder|Classification newQuery()
 * @method static Builder|Classification query()
 * @method static Builder|Classification whereCreatedAt($value)
 * @method static Builder|Classification whereId($value)
 * @method static Builder|Classification whereName($value)
 * @method static Builder|Classification whereSlug($value)
 * @method static Builder|Classification whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int $order
 * @method static Builder|Classification whereOrder($value)
 * @property string|null $description
 * @method static Builder|Classification whereDescription($value)
 */
class Classification extends Model
{
    use HasFactory;
    use HasOtherValue;

    public function answers(): BelongsToMany
    {
        return $this->belongsToMany(Answer::class);
    }

    public function texts(): HasManyThrough
    {
        return $this->hasManyThrough(Text::class, Answer::class);
    }
}

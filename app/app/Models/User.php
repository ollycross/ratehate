<?php

/** @noinspection PhpMissingFieldTypeInspection */

namespace App\Models;

use Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Demographic[] $demographics
 * @property-read int|null $demographics_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Demographic[] $age
 * @property-read int|null $age_count
 * @property-read Collection|Demographic[] $education
 * @property-read int|null $education_count
 * @property-read Collection|Demographic[] $ethnicity
 * @property-read int|null $ethnicity_count
 * @property-read Collection|Demographic[] $gender
 * @property-read int|null $gender_count
 * @property-read Collection|Demographic[] $householdIncome
 * @property-read int|null $household_income_count
 * @property-read Collection|Demographic[] $maritalStatus
 * @property-read int|null $marital_status_count
 * @property-read Collection|Answer[] $answers
 * @property-read int|null $answers_count
 * @property-read string $profile_photo_url
 * @property-read Collection|PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @method static Builder|User whereCurrentTeamId($value)
 * @method static Builder|User whereProfilePhotoPath($value)
 * @method static Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static Builder|User whereTwoFactorSecret($value)
 * @property-read Collection|Comment[] $commentsOnText
 * @property-read int|null $comments_on_text_count
 * @property-read Collection|Incomprehensible[] $incomprehensibles
 * @property-read int|null $incomprehensibles_count
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static Builder|User permission($permissions)
 * @method static Builder|User role($roles, $guard = null)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasRoles;
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'demographics',
        'profile_photo_url',
    ];

    private function getDemographicValue(Demographic $demographic): string
    {
        if (!$demographic->isOther()) {
            return $demographic->name;
        }

        /** @noinspection PhpPossiblePolymorphicInvocationInspection */
        return sprintf('Other: %s', $demographic->otherValue($this->id)->first()->value ?? null);
    }

    public function getDemographicsAttribute()
    {
        $demographics = [];
        $this->demographics()->get()->map(function (Demographic $demographic) use (&$demographics) {
            $demographics[$demographic->getTypeAttribute()] = $this->getDemographicValue($demographic);
        });

        return $demographics;
    }

    public function demographics(): BelongsToMany
    {
        return $this->belongsToMany(Demographic::class);
    }

    private function demographic(string $slug): BelongsToMany
    {
        return $this->demographics()
            ->where(['demographic_type_id', DemographicType::firstWhere('slug', $slug)->id]);
    }

    public function age(): BelongsToMany
    {
        return $this->demographic('age');
    }
    public function ethnicity(): BelongsToMany
    {
        return $this->demographic('ethnicity');
    }
    public function gender(): BelongsToMany
    {
        return $this->demographic('gender');
    }
    public function maritalStatus(): BelongsToMany
    {
        return $this->demographic('marital-status');
    }
    public function householdIncome(): BelongsToMany
    {
        return $this->demographic('household-income');
    }
    public function education(): BelongsToMany
    {
        return $this->demographic('education');
    }

    public function answers(): HasMany
    {
        return $this->hasMany(Answer::class);
    }
    public function incomprehensibles(): HasMany
    {
        return $this->hasMany(Incomprehensible::class);
    }

    public function commentsOnText(): HasMany
    {
        return $this->hasMany(Comment::class)->where('commentable_type', Text::class);
    }

    public function unseenText(): ?Text
    {
        $answered = $this->answers()
            ->select('text_id')
            ->get()
            ->pluck('text_id')
            ->all();

        return Text::inRandomOrder()->limit(1)->whereNotIn('id', $answered)->get()->first();
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }
}

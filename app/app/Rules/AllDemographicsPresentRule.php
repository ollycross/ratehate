<?php

namespace App\Rules;

use App\Exceptions\AllDemographicsPresentRuleException;
use App\Exceptions\DemographicDoesNotExistInTypeException;
use App\Exceptions\DemographicIsNotPresentException;
use App\Models\DemographicType;
use Illuminate\Contracts\Validation\Rule;

class AllDemographicsPresentRule implements Rule
{
    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @param DemographicType $demographicType
     * @param $demographics
     * @return void
     * @throws DemographicDoesNotExistInTypeException
     * @throws DemographicIsNotPresentException
     */
    private function checkDemographicTypeIsPresentAndExists(DemographicType $demographicType, $demographics): void
    {
        $slug = $this->getDemographicSlugOfType($demographicType, $demographics);
        $this->checkDemographicExistsInType($demographicType, $slug);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $demographics
     * @return bool
     * @noinspection PhpMissingParamTypeInspection
     */
    public function passes($attribute, $demographics)
    {
        DemographicType::each(function (DemographicType $demographicType) use ($demographics) {
            try {
                $this->checkDemographicTypeIsPresentAndExists(
                    $demographicType,
                    $demographics
                );
            } catch (AllDemographicsPresentRuleException $e) {
                $this->errors[$e->getDemographicType()->slug] = $e->getMessage();
            }
        });

        return !$this->errors;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return json_encode($this->errors);
    }

    /**
     * @param DemographicType $demographicType
     * @param $demographics
     * @return string
     * @throws DemographicIsNotPresentException
     */
    private function getDemographicSlugOfType(DemographicType $demographicType, $demographics): string
    {
        if (!($slug = $demographics[$demographicType->slug] ?? null)) {
            throw new DemographicIsNotPresentException($demographicType);
        }

        return $slug;
    }

    /**
     * @param DemographicType $demographicType
     * @param string $slug
     * @throws DemographicDoesNotExistInTypeException
     */
    private function checkDemographicExistsInType(DemographicType $demographicType, string $slug): void
    {
        if (
            !$demographicType->demographics()->where('slug', $slug)->exists()
            && !preg_match('/other:/', $slug)
        ) {
                throw new DemographicDoesNotExistInTypeException($demographicType, $slug);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Inertia\Inertia;

class PageController extends Controller
{
    public function index($page)
    {
        return Inertia::render('StaticPage', [
            'page' => Page::where('slug', $page)->firstOrFail()
        ]);
    }
}

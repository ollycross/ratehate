<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Classification;
use App\Models\Comment;
use App\Models\Highlight;
use App\Models\Incomprehensible;
use App\Models\OtherValue;
use App\Models\Text;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Inertia\Response;

class RaterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {

        /** @var Text $text */
        $text = $request->user()->unseenText();

        return $this->renderRater($text);
    }

    private function renderRater(Text $text): Response
    {
        $classifications = Classification::select(['id', 'name', 'slug'])->get()->all();

        return Inertia::render('Index', [
            'text' => $text->only(['id', 'formatted_text']),
            'classifications' => $classifications,
        ]);
    }

    public function processSubmission(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $classificationOtherIsIncluded = in_array('other', $request->post('classificationSlugs') ?: []);

        [
            'textId' => $textId,
            'classificationSlugs' => $classificationSlugs,
            'highlights' => $highlights,
            'classificationOther' => $classificationOther,
            'incomprehensible' => $incomprehensible,
            'comment' => $comment,
        ] = $request->validate([
            'textId' => ['required', 'exists:texts,id', 'integer'],
            'classificationSlugs' => ['array', 'exists:classifications,slug', 'nullable'],
            'highlights' => ['array', 'nullable'],
            'classificationOther' => ['string', 'nullable', Rule::requiredIf($classificationOtherIsIncluded)],
            'incomprehensible' => ['boolean', 'required'],
            'comment' => ['string', 'nullable'],
        ]);


        if ($incomprehensible) {
            $this->markTextAsIncomprehensible($user, $textId);

            return response()->json();
        }

        $this->addAnswer(
            $user,
            $textId,
            $classificationSlugs,
            $highlights,
            $classificationOtherIsIncluded ? $classificationOther : null
        );

        if ($comment) {
            $this->addComment($user, $textId, $comment);
        }

        return response()->json();
    }

    private function markTextAsIncomprehensible(User $user, int $textId)
    {
        Incomprehensible::create([
            'text_id' => $textId,
            'user_id' => $user->id
        ]);
    }

    private function addAnswer(
        User $user,
        int $textId,
        array $classificationSlugs,
        array $highlights,
        ?string $classificationOther = null
    ): Answer {
        $answer = Answer::fromRater($user->id, $textId);

        $classifications = Classification::whereIn('slug', $classificationSlugs)->get();
        $classificationIds = $classifications->pluck('id')->all();

        if ($classificationOther) {
            $other = $classifications->firstWhere('slug', 'other');
            $otherValue = new OtherValue();
            $otherValue->value = $classificationOther;
            $otherValue->relatable_id = $answer->id;
            $otherValue->otherable()->associate($other);
            $otherValue->save();
        }

        $answer->classifications()->syncWithoutDetaching($classificationIds);

        array_map(function ($highlight) use ($answer) {
            return Highlight::fromArray($highlight, $answer->id);
        }, $highlights);

        return $answer;
    }

    private function addComment(User $user, int $textId, string $comment): Comment
    {
        return Comment::create([
            'commentable_id' => $textId,
            'commentable_type' => Text::class,
            'user_id' => $user->id,
            'comment' => $comment,
        ]);
    }
}

<?php

namespace App\Imports;

use App\Models\Text;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithProgressBar;

abstract class TextImport implements ToModel, WithProgressBar, WithHeadingRow, WithBatchInserts
{
    use Importable;

    protected ?string $source;

    /**
     * @param array $row
     * @return string
     */
    protected function getText(array $row): string
    {
        return $row['text'];
    }

    /**
     * @param array $row
     * @return string
     */
    protected function getSource(array $row): string
    {
        return $this->source ?: self::class;
    }

    /**
     * @param array $row
     * @return string
     */
    protected function getSourceId(array $row): string
    {
        return $row['id'];
    }

    public function batchSize(): int
    {
        return 100;
    }

    /**
     * TextImport constructor.
     * @param string|null $source
     */
    public function __construct(string $source = null)
    {
        $this->source = $source;
    }

    /**
     * @param array $row
     * @return Text
     */
    public function model(array $row): Text
    {
        return new Text([
            'text' => $this->getText($row),
            'source' => $this->getSource($row),
            'sourceId' => $this->getSourceId($row),
        ]);
    }

}

<?php

namespace App\Imports\TextImport;

use App\Imports\TextImport;

class RedditImport extends TextImport
{
    protected function getText(array $row): string
    {
        return str_replace("\n", "\n\n", parent::getText($row));
    }
}

<?php /** @noinspection PhpMissingFieldTypeInspection */

namespace App\Console\Commands;

use App\Importer\Reddit;
use App\Imports\TextImport\RedditImport;
use App\Imports\TextRedditImport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class ImportTextCsv extends Command
{
    private const ERROR_CODES = [
        'file_not_found' => 1001,
        'unknown_type' => 1002
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:text {type} {file} {--source=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports a CSV of text entries into the database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filepath = $this->argument('file');
        if (!File::exists($filepath)) {
            $this->output->error(sprintf('File \'%s\' was not found', $filepath));

            return self::ERROR_CODES['file_not_found'];
        }

        $type = $this->argument('type');

        switch ($type) {
            case 'reddit':
                $importerClass = RedditImport::class;
                break;
            default:
                $this->output->error(sprintf('Type \'%s\' is unknown', $type));
                return self::ERROR_CODES['unknown_type'];
        }

        try {
            $importer = new $importerClass($this->option('source'));
            $this->output->title('Starting import');
            $importer->withOutput($this->output)->import($filepath);
            $this->output->success('Import successful');

            return 0;
        } catch (\Exception $e) {
            $this->output->error(sprintf('Importer error (%d): %s', $e->getCode(), $e->getMessage()));

            return self::ERROR_CODES['importer_error'];
        }
    }
}

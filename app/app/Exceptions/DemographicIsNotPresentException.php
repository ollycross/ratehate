<?php

namespace App\Exceptions;

use App\Models\DemographicType;
use Throwable;

class DemographicIsNotPresentException extends AllDemographicsPresentRuleException
{
    /**
     * DemographicIsNotPresentException constructor.
     * @param DemographicType $demographicType
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(DemographicType $demographicType, $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            $demographicType,
            sprintf('Demographic for \'%s\' is not present', $demographicType->name),
            $code,
            $previous
        );
    }
}

<?php

namespace App\Exceptions;

use App\Models\DemographicType;
use Throwable;

class AllDemographicsPresentRuleException extends \Exception
{
    /**
     * @var DemographicType
     */
    private DemographicType $demographicType;

    /**
     * AllDemographicsPresentRuleException constructor.
     * @param DemographicType $demographicType
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(DemographicType $demographicType, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->demographicType = $demographicType;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return DemographicType
     */
    public function getDemographicType(): DemographicType
    {
        return $this->demographicType;
    }
}

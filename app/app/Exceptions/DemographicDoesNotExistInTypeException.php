<?php

namespace App\Exceptions;

use App\Models\Demographic;
use App\Models\DemographicType;
use Throwable;

class DemographicDoesNotExistInTypeException extends AllDemographicsPresentRuleException
{
    /**
     * DemographicDoesNotExistInTypeException constructor.
     * @param DemographicType $demographicType
     * @param string $demographicSlug
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(
        DemographicType $demographicType,
        string $demographicSlug,
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct(
            $demographicType,
            sprintf('DemographicType \'%s\' has no Demographic \'%s\'', $demographicType->name, $demographicSlug),
            $code,
            $previous
        );
    }
}

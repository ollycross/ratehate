<?php

namespace App\Helper;

class Helper
{
    /**
     * Converts an ordered Yaml array parsed from a list like:
     *
     * ```
     * - Group1:
     *   - Group1.1
     *   - Group1.2
     *   - Group1.3
     * - Group2:
     *   - Group2.1
     *   - Group2.2
     *   - Group2.3
     * ```
     *
     * To an indexed PHP array like:
     *
     * ```
     * [
     *     'Group1' => [
     *         'Group1.1',
     *         'Group1.2',
     *         'Group1.3'
     *     ],
     *     'Group2' => [
     *         'Group2.1',
     *         'Group2.2',
     *         'Group2.3',
     *     ],
     * ]
     * ```
     *
     * Note we pass in the Yaml ARRAY not the string to allow conversion of smaller chunks of larger arrays
     *
     * @param array $yamlArray
     * @return array
     */
    public static function orderedYamlFileToIndexedArray(array $yamlArray): array
    {
        $out = [];
        foreach ($yamlArray as $entry) {
            foreach ($entry as $key => $value) {
                $out[$key] ??= [];
                $out[$key][] = $value;
            }
        }

        return $out;
    }
}

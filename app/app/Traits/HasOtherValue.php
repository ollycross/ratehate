<?php

namespace App\Traits;

use App\Models\OtherValue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasOtherValue
{
    public function isOther(): bool
    {
        return $this->slug === 'other';
    }

    public function otherValue(int $relatableId): MorphOne
    {
        return $this->morphOne(OtherValue::class, 'otherable')->whereRelatableId($relatableId);
    }

    public static function fromSlug(string $slug, ?Builder $builder = null, ?int $relatableId = null)
    {
        $builder = $builder ?? self::query();

        if (!preg_match('/^other:(.*)/', $slug, $match)) {
            return $builder->firstWhere('slug', $slug);
        }

        if ($relatableId) {
            [, $otherValueText] = $match;
            $other = $builder->firstWhere('slug', 'other');

            $otherValue = new OtherValue();
            $otherValue->value = $otherValueText;
            $otherValue->relatable_id = $relatableId;

            $otherValue->otherable()->associate($other);
            $otherValue->save();

            return $other;
        }

        return null;
    }
}

<?php

namespace App\Traits;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasComment
{
    public function comment(): MorphOne
    {
        return $this->morphOne(Comment::class, 'commentable');
    }
}

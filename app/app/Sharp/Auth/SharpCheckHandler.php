<?php

namespace App\Sharp\Auth;

use App\Models\User;
use Code16\Sharp\Auth\SharpAuthenticationCheckHandler;

class SharpCheckHandler implements SharpAuthenticationCheckHandler
{
    public const REQUIRED_PERMISSION = 'sharp';

    /**
     * @param $user
     * @return bool
     */
    public function check($user): bool
    {

        /** @var User $user */
        return $user->hasPermissionTo(self::REQUIRED_PERMISSION);
    }
}

<?php

namespace App\Sharp\Page;

use App\Models\Page;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormWysiwygField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\SharpForm;

class PageForm extends SharpForm
{
    use WithSharpFormEloquentUpdater;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        return $this->transform(
            Page::findOrFail($id)
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        $page = $id ? Page::findOrFail($id) : new Page();
        $this->save($page, $data);

        return $page->id;
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Page::findOrFail($id)->find($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('title')->setLabel('Title')
        );
        $this->addField(
            SharpFormTextField::make('slug')->setLabel('Slug')
        );
        $this->addField(
            SharpFormWysiwygField::make('content')
            ->setLabel('Content')
            ->setToolbar([
                SharpFormWysiwygField::B,
                SharpFormWysiwygField::I,
                SharpFormWysiwygField::UL,
                SharpFormWysiwygField::OL,
                SharpFormWysiwygField::SEPARATOR,
                SharpFormWysiwygField::A,
                SharpFormWysiwygField::H1,
                SharpFormWysiwygField::CODE,
                SharpFormWysiwygField::QUOTE,
                SharpFormWysiwygField::INCREASE_NESTING,
                SharpFormWysiwygField::DECREASE_NESTING,
                SharpFormWysiwygField::UNDO,
                SharpFormWysiwygField::REDO,
            ])
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFields('title', 'slug', 'content');
        });
    }
}

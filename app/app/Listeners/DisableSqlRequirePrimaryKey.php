<?php

namespace App\Listeners;

use Illuminate\Support\Facades\DB;

class DisableSqlRequirePrimaryKey
{
    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle()
    {
        DB::statement('SET sql_require_primary_key=false');
    }
}

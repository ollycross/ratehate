import rangy from 'rangy'

const VueRangy = {
    install(Vue, options) {
        if (options.hasOwnProperty('init') && options.init) {
            rangy.init()
        }

        Vue.prototype.$rangy = rangy
    },
}

export default VueRangy

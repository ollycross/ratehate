import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { faQuestionCircle, faCheckCircle, faCircle } from '@fortawesome/free-solid-svg-icons'

library.add(faQuestionCircle)
library.add(faCheckCircle)
library.add(faCircle)

Vue.component('FontAwesomeIcon', FontAwesomeIcon)

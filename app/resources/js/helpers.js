const helpers = {
    getFormErrors: (errors, names, notGeneric = []) => {
        const error2String = (error) => (Array.isArray(error) ? error.join('; ') : error)
        const formErrors = {}

        names.forEach((name) => {
            if (name in errors) {
                formErrors[name] = error2String(errors[name])
            }
        })

        formErrors.generic = _.difference(Object.keys(errors), names, notGeneric).map((name) =>
            error2String(errors[name])
        )

        return formErrors
    },
    getFormVariants: (names, errors) => {
        const formVariants = {}
        names.forEach((name) => {
            formVariants[name] = errors[name] ? 'danger' : null
        })

        return formVariants
    },
}

module.exports = helpers

require('./bootstrap')

import Vue from 'vue'

import { InertiaApp } from '@inertiajs/inertia-vue'
import { InertiaForm } from 'laravel-jetstream'
import PortalVue from 'portal-vue'
import VueRangy from './plugins/vue-rangy'
import Loading from 'vue-loading-overlay'
import './config/vue-font-awesome'
import './config/vue-tailwind-settings'

import Popper from 'vue-popperjs'
Vue.use(InertiaApp)
Vue.use(InertiaForm)
Vue.use(PortalVue)

Vue.component('Popper', Popper)
Vue.component('Loading', Loading)
Vue.use(VueRangy, {
    init: true,
})
// noinspection JSUnresolvedVariable
Vue.mixin({ methods: { route } })

const app = document.getElementById('app')

new Vue({
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app)

/* eslint-disable prettier-vue/prettier */
const defaultTheme = require('tailwindcss/defaultTheme');

const COLUMN_LAYOUT = 12;
const TO_COLUMNS = ['maxWidth', 'minWidth'];

const _ = require('lodash');

const config = {
    purge: {
        content: [
            './vendor/laravel/jetstream/**/*.blade.php',
            './storage/framework/views/*.php',
            './resources/views/**/*.blade.php',
            './resources/js/**/*.vue',
            './resources/js/config/vue-tailwind-settings.js',
        ]
    },

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                brand: {
                    50: '#F3F3F5',
                    100: '#E6E8EB',
                    200: '#C1C5CD',
                    300: '#9CA2AF',
                    400: '#515D73',
                    500: '#071737',
                    600: '#061532',
                    700: '#040E21',
                    800: '#030A19',
                    900: '#020711',
                },
                cta: {
                    50: '#F5F3F3',
                    100: '#EBE7E6',
                    200: '#CDC3C1',
                    300: '#AF9F9C',
                    400: '#735751',
                    500: '#370F07',
                    600: '#320E06',
                    700: '#210904',
                    800: '#190703',
                    900: '#110502',
                },
                info: {
                    50: '#F5FBFD',
                    100: '#EAF7FB',
                    200: '#CBECF4',
                    300: '#ABE0ED',
                    400: '#6CC8E0',
                    500: '#2DB1D3',
                    600: '#299FBE',
                    700: '#1B6A7F',
                    800: '#14505F',
                    900: '#0E353F',
                },
                warning: {
                    50: '#FDFBF4',
                    100: '#FAF7EA',
                    200: '#F3EBCA',
                    300: '#EBDFAB',
                    400: '#DCC86B',
                    500: '#CDB02C',
                    600: '#B99E28',
                    700: '#7B6A1A',
                    800: '#5C4F14',
                    900: '#3E350D',
                },
                success: {
                    50: '#F5FCF6',
                    100: '#EBF9ED',
                    200: '#CEEFD1',
                    300: '#B0E5B6',
                    400: '#74D27F',
                    500: '#39BE48',
                    600: '#33AB41',
                    700: '#22722B',
                    800: '#1A5620',
                    900: '#113916',
                },
                danger: {
                    50: '#FDF5F6',
                    100: '#FAEAEC',
                    200: '#F3CCD0',
                    300: '#EBADB4',
                    400: '#DD6F7B',
                    500: '#CE3143',
                    600: '#B92C3C',
                    700: '#7C1D28',
                    800: '#5D161E',
                    900: '#3E0F14',
                },
            },
        },
    },

    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },

    plugins: [
        require('@tailwindcss/ui'),
        require('@tailwindcss/custom-forms'),
        require('tailwindcss-filters'),
    ],
};

TO_COLUMNS.forEach((property) => {
    config.theme.extend[property] = config.theme.extend[property] || {}
    _.range(1, COLUMN_LAYOUT + 1).forEach((divisor) => {
        config.theme.extend[property][`${divisor}/${COLUMN_LAYOUT}`] = `${
            (divisor / [COLUMN_LAYOUT]) * 100
        }%`;
    })
});

module.exports = config;

FROM thecodingmachine/php:7.4-v3-apache

RUN sudo apt install -y openssl

RUN sudo mkdir -p /etc/ssl/private /etc/ssl/certs && \
	sudo openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj "/C=UK/ST=Docker/L=Docker/O=OllyOllyOlly/CN=local.ratehate.com" -keyout /etc/ssl/private/ssl-cert-snakeoil.key -out /etc/ssl/certs/ssl-cert-snakeoil.pem

RUN sudo a2ensite default-ssl

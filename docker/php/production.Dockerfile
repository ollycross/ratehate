ARG PHP_EXTENSIONS='bcmath gd pdo_mysql'

FROM thecodingmachine/php:7.4-v3-apache-node12 AS build-stage

ENV PHP_EXTENSION_GD=1
ENV APP_ENV=production

WORKDIR /var/www
RUN sudo chown -R docker:docker .
RUN rm -rf ./html
COPY --chown=docker:docker app/composer.* ./
COPY --chown=docker:docker app/package* ./

RUN composer install \
        --prefer-dist \
        --no-scripts \
        --no-dev \
        --no-autoloader

RUN npm install

COPY --chown=docker:docker app .

RUN composer dump-autoload -o

RUN npm run production

FROM thecodingmachine/php:7.4-v3-slim-apache as latest

ARG TRUSTED_PROXIES

ENV APP_NAME="Rate the Hate"

ENV APACHE_RUN_USER=www-data
ENV APACHE_RUN_GROUP=www-data
ENV APACHE_EXTENSION_SOCACHE_SHMCB=1
ENV APACHE_EXTENSION_SSL=1
ENV APACHE_EXTENSION_REMOTEIP=1
ENV APACHE_DOCUMENT_ROOT='/var/www/public/'

RUN sudo bash -c "echo -e 'RemoteIPTrustedProxy $TRUSTED_PROXIES' > /etc/apache2/conf-available/remoteip.conf" && \
    sudo a2enconf remoteip && \
    sudo service apache2 restart

WORKDIR /var/www
COPY --from=build-stage /var/www .
RUN sudo chown -R www-data:www-data /var/www/storage
